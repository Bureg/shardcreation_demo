﻿using UnityEngine;
using System.Collections;

public class FreezeController : MonoBehaviour
{

    private bool m_isFreezed;
	// Update is called once per frame
	private void Update () {

	    if (Input.GetKeyDown(KeyCode.Space))
	    {
	        if (m_isFreezed)
	        {
	            Time.timeScale = 1;
	            m_isFreezed = false;
	        }
	        else
	        {
                Time.timeScale = 0;
                m_isFreezed = true;
	        }
	    }
	}
}
