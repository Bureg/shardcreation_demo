﻿using UnityEngine;

public class LifeTime : MonoBehaviour
{

    [SerializeField]
    private float m_lifeTime = 2.0f;

    private void Update () {

	    if (m_lifeTime <= 0)
	    {
            Destroy(this.gameObject);
        }
        m_lifeTime -= Time.deltaTime;
	}
}
