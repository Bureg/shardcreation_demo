﻿using UnityEngine;
using System.Collections;

public class MouseTracking : MonoBehaviour {

    // Позаимствованный скрипт

    [SerializeField]
    private enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    [SerializeField]
    private RotationAxes axes = RotationAxes.MouseXAndY;
    [SerializeField]
    private float sensitivityX = 15F;
    [SerializeField]
    private float sensitivityY = 15F;


    [SerializeField]
    private float minimumY = -60F;
    [SerializeField]
    private float maximumY = 60F;

    private float rotationY = 0F;

    void Update()
    {

        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
        }
        else
        {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
        }
    }
}

