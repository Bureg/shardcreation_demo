﻿using UnityEngine;
using System.Collections;

public class ShardsCreator : MonoBehaviour
{

    [SerializeField]
    private GameObject m_shardPref;
    [SerializeField]
    private int m_maxShardCount = 10;
    [SerializeField]
    private int m_minShardCount = 5;
    [SerializeField]
    private float m_impulseFactor = 0.1f;

    private void OnCollisionEnter(Collision col)
    {
        // Нужно отгородится каким-то более разумным способом от нежелательных срабатываний, возможно использовать слои
        if (col.gameObject.tag != "Shard" && col.gameObject.tag != "Ground")
        {
            foreach (ContactPoint contact in col.contacts)
            {
                // количество осколков можно сделать зависимым от силы удара о поверхность
                int shardsToCreate = Random.Range(m_minShardCount, m_maxShardCount);
                for (int i = 0; i < shardsToCreate; i++)
                {
                    
                    var shard = Instantiate(m_shardPref, contact.point, Quaternion.identity) as GameObject;
                    Rigidbody shardRb = shard.GetComponent<Rigidbody>();
                    shardRb.rotation = Random.rotation;
                    shardRb.AddForce(col.impulse * m_impulseFactor, ForceMode.Impulse);
                }
            }
        }
    }
    

}
