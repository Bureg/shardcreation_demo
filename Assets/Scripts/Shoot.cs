﻿using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField]
    private float m_power = 10;

    private void Awake()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * m_power, ForceMode.Impulse);
    }
    
}
