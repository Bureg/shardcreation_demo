﻿using UnityEngine;
using System.Collections;

public class ShootingController : MonoBehaviour {

    [SerializeField]
    private Transform m_bullet;

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(m_bullet, transform.position, transform.rotation);
        }
    }
}
